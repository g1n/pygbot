import json

def changeobj(config, obj, value):
    config[obj] = value
    writeconfig("config.json", config)
    return config    

def readobj(config, obj):
    return config[obj]

def writeconfig(configfile, data):
    with open(configfile, 'w') as f:
        json.dump(data, f)

def readconfig(configfile):
    with open(configfile, 'r') as f:
        config = json.load(f)
    return config

class pygbot:
    nick = ""
    host = ""
    port = 0 
    tls = False
    prefix = ""
    channels = []
    admins = []

