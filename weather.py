import requests

def getweather(bot, channel, message):
    try:
        location = message.split()[1:]
        page = requests.get(f"http://wttr.in/{'+'.join(location)}?format=%l+|+%t+|+%C+|+Humidity:+%h+|+Wind:+%w")
        bot.send(channel, page.text)
    except:
        bot.send(channel, "Failed to get weather")
