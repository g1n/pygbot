#!/usr/bin/python3

import time
import signal
import irc
import commands
import config as c

config = c.readconfig("config.json")

nick = c.pygbot.nick = c.readobj(config, "nick")
host = c.pygbot.host = c.readobj(config, "host")
port = c.pygbot.port = c.readobj(config, "port")
tls = c.pygbot.tls = c.readobj(config, "tls")
channels = c.pygbot.channels = c.readobj(config, "channels")
admins = c.pygbot.admins = c.readobj(config, "admins")
prefix = c.pygbot.prefix = c.readobj(config, "prefix")

bot = irc.IRC()

signal.signal(signal.SIGINT, bot.shutdown)

bot.connect(host, port, nick, nick, nick, None, tls)
time.sleep(1)
for channel in channels:
    bot.join(channel)

line = ''

while True:
    resp=bot.get_response()
    if resp != '\n' and resp != '\r':
        line += resp
        print(resp, end='')
    else:
        print('\r\n', end='')
        parsedline = bot.command_parser(line)
        if len(parsedline) > 1:
            user, server_command, channel, message = parsedline[0][1:], parsedline[1], parsedline[2], ' '.join(parsedline[3:])[1:]
            commands.bot_command_parser(config, bot, user, channel, message)
        line = ''
    

