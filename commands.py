import re
import irc
from config import pygbot as c
from config import changeobj
import youtube as yt
import web
import weather as w

def bothelp(bot, channel):
    helpmsg = """Hi I am pygbot. Created by g1n. My commands: help, ping, echo, yt (youtube), t (title), w (weather) but more features are planing"""
    bot.send(channel, helpmsg)

def isadmin(user):
    if user in c.admins:
        return True
    else:
        return False

def bot_command_parser(config, bot, user, channel, message):
    if channel == "pygbot":
        channel = user.split('!')[0]
    if message[0] == c.prefix: 
        if message.split()[0] == (c.prefix + "echo"):
            bot.send(channel, ' '.join(message.split()[1:]))
        elif message.split()[0] == (c.prefix + "admin"):
            if isadmin(user):
                bot.send(channel, "You are admin")
            else:
                bot.send(channel, "You are not admin")
        elif message.split()[0] == (c.prefix + 'c') or message.split()[0] == (c.prefix + 'config'):
            configure(config, bot, channel, message) if isadmin(user) else bot.send(channel, "Permissions Denied")
        elif message.split()[0] == (c.prefix + "ping"):
            bot.send(channel, "pong")
        elif message.split()[0] == (c.prefix + "help"): 
            bothelp(bot, channel)
        elif message.split()[0] == (c.prefix + "join"):
            bot.join(message.split()[1]) if isadmin(user) else bot.send(channel, "Permissions Denied")
        elif message.split()[0] == (c.prefix + "kick"):
            bot.kick(channel, message.split()[1]) if isadmin(user) else bot.send(channel, "Permissions Denied")
        elif message.split()[0] == (c.prefix + "eval"):
            boteval(config, bot, channel, message) if isadmin(user) else bot.send(channel, "Permissions Denied")
        elif message.split()[0] == (c.prefix + "exec"):
            botexec(config, bot, channel, message) if isadmin(user) else bot.send(channel, "Permissions Denied")
        elif message.split()[0] == (c.prefix + "quit"):
            bot.shutdown(None, None, "bye") if isadmin(user) else bot.send(channel, "Permissions Denied")

        elif message.split()[0] == (c.prefix + "yt") or message.split()[0] == (c.prefix + "youtube"):
            yt.gettitle(bot, channel, message)
        elif message.split()[0] == (c.prefix + "t") or message.split()[0] == (c.prefix + "title"):
            web.gettitle(bot, channel, message)
        elif message.split()[0] == (c.prefix + "w") or message.split()[0] == (c.prefix + "weather"):
            w.getweather(bot, channel, message)
    else:
        parseiflink(bot, channel, message)

def parseiflink(bot, channel, message):
    for i in message.split():
        messagehaslink = re.search("http.*://", i)
        if messagehaslink != None:
            isyoutubelink = re.search("youtu.*be.*", i)
            if isyoutubelink != None:
                yt.sendyttitle(bot, channel, i, False)
            else:
                web.sendtitle(bot, channel, i, False)

def configure(config, bot, channel, message):
    try:
        message = message.split()
        if message[1] == "bot":
            if message[2] == "prefix":
                changeobj(config, "prefix", message[3])
                c.prefix = message[3]
            elif message[2] == "nick":
                bot.setnick(message[3])
                changeobj(config, "nick", message[3])
                c.nick = message[3]
        else:
            bot.send(channel, "No such config category")
            return 1
        bot.send(channel, "Changed successfully")
    except:
        bot.send(channel, "Something went wrong")

def boteval(config, bot, channel, message):
    try:
        eval(str(''.join(message.split()[1:])))
    except:
        bot.send(channel, "Failed to eval")

def botexec(config, bot, channel, message):
    try:
        exec(str(' '.join(message.split()[1:])))
    except:
        bot.send(channel, "Failed to exec")
