import re
import requests
from bs4 import BeautifulSoup

def gettitle(bot, channel, message):
    try:
        link = ' '.join(message.split()[1:])
        iflinkok = re.search("^http.*://", link)
        if iflinkok != None:
            sendyttitle(bot, channel, link, True)
        else:
            bot.send(channel, "Failed to get title")
    except:
        bot.send(channel, "Failed to get title")

def sendyttitle(bot, channel, link, sendonfailed):
    try:
        page = requests.get(link)
        soup = BeautifulSoup(page.text, 'html.parser')
        title = soup.find("meta", itemprop="name")["content"]
        bot.send(channel, title)
    except:
        if sendonfailed == True:
            bot.send(channel, "Failed to get title")

